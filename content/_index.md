---
title: About me
---

## Hey there! &#128075; 

I'm **Aiden Fox-Ivey**, a computer engineering student at the University of Waterloo.

<center>
<img src="images/aiden_head.png" style="border:5px black;" width="150" height="150" alt="Photo of my face.">
</center>

I'm competent with C, C++, Rust, and Python. You can find work I've done with
these languages in my github account below. I typically opt to use C++ for
schoolwork and programming competitions for its speed and object oriented
features, C for embedded work and personal projects, Rust for CLI apps, and
Python for design team work or any quick scripting that needs to be done. I
have a decent sense of traditional design patterns for Python, Rust, and C,
whereas C++ design pattern knowledge for me is still a skill in development.

I'm also a huge GNU/Linux fan, having used it for a few years now. I started
off with Ubuntu-based distros (Elementary OS, PopOS!), later transitioning to
using Fedora, Gentoo, and Artix interchangebly. Although I'm most competent
with the install process and security of modern x86\_64 machines, I've worked
with 32 bit and ARM64 computers in the past.

In terms of data science, and more analytical workloads, I've used Matploblib, Numpy,
Pandas, Tensorflow, Sci-Kit Learn, OpenCV, and NLTK. I can pretty quickly pick up 
new libraries and technology to become up to speed.

Headless deployment of embedded computers running Linux is also something I've
done in the past. The latest focus of the deployment side of my interests in
Docker. My homelab runs most of its software in containers in order to avoid
messing up the underlying FreeBSD system.

In regards to hardware, I'm in the midst of building Ben Eaters's 8-bit CPU
project. It essentially consists of a series of ICs and breadboard components
placed together to form the essential parts of a CPU (ALU, RAM, clock). If
you're interested in building it, information can be found
[here](https://eater.net/8bit/).

In addition to the work I've done with basic ICs and discrete logic gates, I've
used a variety of kinds of microcontrollers. PIC16 at Waterloo Rocketry, STM32
for coursework at UW, and Arduinos for hackathons. I won't make any claims of
being an MCU guru per se, but I know enough to get one to work in a decent
project.

If you're interested in contacting me for internship opportunities, need some help
on a project, or just want to chat about techie stuff, check below. 

Aiden
<br>
<br>
### Here's my [PGP key](./pub.pgp).

### Here are a few of the ways to reach me:
- [Email](mailto:aiden@foxivey.dev)
- [Matrix](https://matrix.to/#/@aidenfoxivey:matrix.org)
- [LinkedIn](https://www.linkedin.com/in/aidenfoxivey/)

### Projects and Hackathons
- [github.com/aidenfoxivey](https://github.com/aidenfoxivey)
- [gitlab.com/aidenfoxivey](https://gitlab.com/aidenfoxivey)
- [devpost.com/Foxnaut](https://devpost.com/Foxnaut)

### Competitive Programming
- [dmoj.ca/user/aidenfoxivey](https://dmoj.ca/user/aidenfoxivey)
- [open.kattis.com/users/aidenfoxivey](https://open.kattis.com/users/aidenfoxivey)
- [spoj.com/users/aidenfoxivey](https://www.spoj.com/users/aidenfoxivey/)

### Posts
