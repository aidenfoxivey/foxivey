---
title: Books
draft: false
---

Below is a list of books that I've either read or want to read.

They should serve as a good idea of what kind of topics I've been exposed to and focused on.


Have read:
- C Programming Language (K&R)
- Programming in C 4ed (Kochan)
- Think Python

Am reading:
- Algorithmic Thinking
- Rust Programming Language 2018 ed.
- Mathematics for Machine Learning 

Want to read:
- Clean Code
- The Structure and Interpretation of Computer Programs
- Design Patterns
- The Mythical Man Month
- The Cathedral and the Bazaar
- Introduction to Algorithms
- The Elements of Computing Systems: *Building a Modern Computer from First Principles*


To that point, if you do ever find a good source of used copies of the books under "Want to read", I would be 
very grateful if you contacted me with details.
