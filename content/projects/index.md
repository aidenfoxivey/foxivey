---
draft: false
title: Projects
---

---
Oxidized
---
**Language**: Rust  
**Libraries**: StructOpt  
**Purpose**: Recreate the ed text editor in Rust.  
**Status**: <span style="color:blue">In Progress...</span>   
**Source Code**: https://gitlab.com/aidenfoxivey/oxidized

<details>
    <summary> See more details! </summary>

After accidentally stumbling into ed while messing around on my computer, I
learned that there existed a text editor so unfriendly that it made Vim pale by
comparison. 

I quickly realized that the reason for keeping the output limited to a handful
of lines at a time was due to historical computers printing their output rather
than displaying it on a monitor as I typically prefer to do. 

A few of the features I wanted to include in my implementation was Regex
parsing, a decent commandline interface, and a faithful representation of the
old project. The commandline interface was easily handled by a library called
StructOpt, which in turn was a wrapper around Clap. 

Clap is essentially a library with a builder syntax constructor for apps that
automatically formats a basic command line interface with minimal
configurations required by the author. StructOpt takes that one step further by
parsing a struct that represents the inputs to the CLI app and automatically
writes the options, switches, and inputs.

```Rust
#[derive(Debug, StructOpt)]
#[structopt(name = "rust_ed", about = "A Rust implementation of GNU ed.")]
struct Arg {
    /// Check version and license info
    #[structopt(short = "V", long = "version")]
    version: bool,

    /// Quiet output (without byte counts)
    #[structopt(short, long)]
    quiet: bool,

    // be verbose as for byte counts, etc.
    /// Verbosity (-v, -vv , -vvv, etc.)
    #[structopt(short, long, parse(from_occurrences))]
    verbosity: u8,

    /// Path of file to edit
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}
```
The most recent predicament is that of the parser, or lack thereof. Currently,
I have essentially no good way to take patterns out of text. Eventually, I will
likely opt to use some sort of wildcards in a pattern, but the overall
structure of commands makes it difficult to only handle some cases versus
others.

</details>

--- 
CourseScape 
---

**Languages**: JavaScript, HTML, CSS  
**Libraries**: MongoDB, React, Bootstrap  
**Tools**: Docker  
**Purpose**: Create a crowdsourced "rate your class" kind of
experience  
**Source Code**: https://github.com/aidenfoxivey/Coursescape

<details> 
    <summary> See more details! </summary>

Having seen some of the projects that my classmates made using web
technologies, I was encentivized to try something flashy and new, like React
makes one feel.

My knowledge of using the web primarily comes from static site generators like
Hugo and Jekyll, so it was certainly different to take ideas like routing into
account. The backend for the application was done in JS as was the frontend,
which made it arguably a good exercise in expanding my JavaScript knowledge. I
opted to use MongoDB as a database, having heard the benefits of it extolled
earlier by some excited classmates. 

I used MongoDB Atlas purely for the ease of relying on a pre-configured system
to work with, but look forward to using a manually configured version in the
future. 

The last feature I ensured to include was a Dockerfile at the base of the repository.
I minimized the dependencies used on the application by opting to use trimmed down
versions of applications to build the docker image, like Alpine GNU/Linux as opposed
to something large like Ubuntu.
</details>
