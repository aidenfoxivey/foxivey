---
title: "Lazeez Review"
date: 2022-04-23
draft: false
---

(The following is not a defamation of Lazeez Shawarma Canada.)

I had heard of Lazeez from the UW subreddit about a year prior to coming here. Interesting that I knew about 
what was considered "university culture" before knowing what I wanted to apply to the university for.

I heard legends about the lines that would leave your gastrointestinal system begging for mercy and the tumultuous
rivers of egg-infused garlic sauce. In some sense, I think they're perhaps a metaphor for the co-op experience at
the university. A careful explanation that no matter how well you think you might be doing, you're really just like a 
logdriver on precarious white water, of course with the dry (somewhat cardboard-like) fries on top of that god awful
garlic sauce.

Regardless of what I think it stood for metaphorically, I had not actually had any, which felt unfair to my status as 
being a student here.

Well today I changed that. I purchased a small "Falafel on the Sticks" dish with tahini on top. Whatever I had
was a particularly strange combination of food that involved my girlfriend (who was there with me at the time)
to vomit what she described as "nothing much, just a few tablespoons". 

Maybe I'm missing something about what Lazeez stands for. The background and core values of Lazeez very
possibly evade me, leaving me and my naivete all alone. Am I even a UW student considering my lackluster 
impressions of Lazeez?

 
