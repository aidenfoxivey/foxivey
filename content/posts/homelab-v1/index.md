--- 
title: "Homelab v1" 
date: 2022-04-12
draft: false 
---

# Building my Homelab v1

I picked up an old SuperMicro server earlier this coop term. My brother had
found it on Kijiji under "Server/NAS" and mentioned that it was a good deal
given the number of drive slots on the case. I was enthused at the idea of
having something to run Kubernetes and an SFTP server off of. I'm not the
biggest collector of spare bytes, but since seeing Jeff Geerling's
[video](https://google.com) and rules on making solid backups, the rules goblin
inside of me decided that having a NAS was priority number one.

## Why a used server?

Well, that's a good question. You can't really argue with a $50 server give the
price of computers these days. The largest motivation to not get an off the
shelf NAS (IE Synology) is probably the price. It seemed kind of ridiculous to
pay that much for the specs offered. Admittedly, I'm not naive enough to think
there is no advantage. The power consumption on my NAS will definitely outpace
that of what you can get on an off the shelf one, but it's cheap enough for me
to not really care.

The second competitor for this would probably be some sort of ARM SBC. (Don't
even tell me about the cursed x86 SBCs, they scare me.) Honestly, I might have
purchased an ODroid or Pine64 board if they had better connectivity and were
around the same price/performance. I am a big fan of ARM processors given their
lower average power consumption and their portable size. The main pain point for
me was that finding a board with good SATA connectivity. Something about running
all that data over USB3.0 just doesn't seem right.

## Specs

**CPU**: AMD FX-8350  
**GPU**: some weird board I don't 100% recognize  
**RAM**: 16 GB DDR3   
**Motherboard**: ASUS M5A97 LE R2.0   
**Storage**: 2 x 4 TB Ironwolf NAS Drives + a handful of junky 2.5" drives from old laptops  

The PC came in a massive case. It is definitely the biggest computer I have ever
owned. Somehow it manages to be incredibly deep and also wide. I am not an
expert on PC case sizes, but I believe it's somewhere north of an ATX case.
Inside, the cables are not necessarily organized, but that's not necessary for a
workhorse like this.

![The case.](/images/nas.jpg)  

As for the PCIE cards, it came with that odd looking GPU. I assumed it was an
NVIDIA card, but it says AMD on the side, so I'm not entirely sure what to
believe.

![The GPU.](/images/graphics-card.jpg) 

It also had a little card for a networking switch with four gigabit ethernet
ports. I can only assume at the time it was purchased, that was one pricey add
in card. I don't see myself using the ports, but they're a nice to have
certainly.

![The network switch.](/images/pcie-switch.jpg) 

## Setting it up

I opted to use TrueNAS just because it seemed reasonable to have good defaults
rather than have me manually set something up the first time given how much a
pain it is to drag it downstairs where my monitor lives for any maintenance
work. Perhaps that's something a Raspberry Pi would do better... Anyways, I used
the server for a bit with a 2.5" HDD, but found it was too slow for my liking.

I then opted to use a spare NVME drive I have. The motherboard doesn't actually
have a space for an M.2 drive, so I figured an adapter would probably be a wise
idea. I went out and purchased one from the Canada Computers store near the
University of Waterloo. It didn't take long to install it when I plugged it in.
It's funny how much an SSD speeds up the most mundane actions. 

![The nvme drive.](/images/nvme-carrier.jpg) 

Then I got it, the wonderful message of "blah blah blah CSM module doesn't like
you" popped up. That's great isn't it. Just lovely when something decides to
step in your way of the first homelab you'll ever make. That said, I think it's
an imperative exercise in computing to have your computer brick itself. (To be
clear, it's often operator error... ;) ) 

## SSD Issues

As usual, it turns out that something about the motherboard's bios did not 
want to cooperate with the PCIE card holding the NVME drive I had. I just about 
had it at this point. So much fiddling around with getting FreeNAS to install
on one drive and not the others had made me somewhat irate. This was seeming to 
be turning out in such a way that I was not pleased with it. I almost considered
stripping the server for parts, selling them on Ebay and then promptly purchasing
a Rockchip powered SBC. After poking around, I located another SSD that I could 
use for this. It's a regular old SATA, so not quite breakneck speeds, but again,
for a NAS is just fine. Thanks to Canada Computers return policy, I was able to 
recoup the 15 dollars I spent on said PCIE board.

## Operator Error 
I've been into distributions of GNU/Linux for a while now. I think I started
with Linux Mint when I was about 12 or so and then took a long break until my 
final year of highschool. Truth be told, there was a really long period of time 
where I didn't own a computer. I spent a large amount of time just using the 
internet for entertainment and playing games. Realistically, I entered the world
of GNU/Linux about a year ago now, when I tried ElementaryOS. Admittedly, it 
wasn't the most wonderful distribution to start with, but you live and learn.
After messing around with it in a VM, I decided to install it on a harddrive.
5400 rpm of spinning metal is a slow speed for someone coming from Windows 10 
on an NVME. I hilariously had the impression that ElementaryOS was just a really
slow experience, despite the fact that I was running it on a disk drive. For 
some strange reason, I persisted and went through the horrible process of dual 
booting Windows and GNU/Linux.

The TL;DR of that is that I have some experience with Linux. Now, I  keep hearing
all the time that the real meta is to [use FreeBSD](https://unixsheikh.com/articles/the-flaws-of-distro-hopping-and-asking-other-people-about-their-os-of-choice.html).
Whether or not that is actually correct, I now feel incentivized to give it a try.
I poked around a bit with FreeNAS, and might try with XigmaNAS, but ultimately 
minimalism seems to be a good idea if your application doesn't need to be incredibly
secure and you have a few weekends to poke about the application. 

I'll probably end up writing a little bit once I properly try FreeNAS, but for now 
I have the FreeBSD docs to attend to.


/* continue on from here */
